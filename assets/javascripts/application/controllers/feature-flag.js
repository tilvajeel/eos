/* ==========================================================================
   Manipulate feature flaged elements from the FE
   ========================================================================== */
$(function () {
  /**
   * Use this as follows:
   *
   * Add the class .js-feature-flag to an item to want to verify with
   * feature flag.
   * You should also include the data attribute feature-flag like
   * `data-feature-flag= docEnabled['/path/of-feature']`.
   * If the feature is enabled it will be shown, if not, it will get the
   * `hidden` class. Features undefined will still be shown as it is assumed
   * there is some missing data from the DB but the feature should be Feature Flagged
   */
  $.each($('.js-feature-flag'), function () {
    const $self = $(this)
    const ff = $(this).data('feature-flag')
    if (ff !== undefined) {
      if (!ff.enabled) {
        // if it is a slick galery item we need to delete it manually
        if ($self.data('slick-index')) {
          const slickIndex = $self.data('slick-index')
          $('.js-ag-carousel').slick('slickRemove', slickIndex)
        } else {
          $self.detach()
        }
      }
    }
  })
})
