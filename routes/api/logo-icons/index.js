const express = require('express')
const router = express.Router()
const fs = require('fs')

/* ==========================================================================
   Generate a single object from icon-svg folder
   ========================================================================== */

const dir = `assets/images/logo-icons/`
let dataLogoIcon = []

dataLogoIcon = fs.readdirSync(dir)
router.get('/', (req, res, next) => {
  res.status(200).json(dataLogoIcon)
})

module.exports = router
